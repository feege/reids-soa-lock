package com.gaofei.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : gaofee
 * @date : 20:05 2021/1/26
 * @码云地址 : https://gitee.com/itgaofee
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisLockTestAnnotation {
    String redisKey();
}
