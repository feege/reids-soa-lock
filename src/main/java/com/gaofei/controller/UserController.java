package com.gaofei.controller;

import com.alibaba.fastjson.JSONObject;
import com.gaofei.anno.RedisLockTestAnnotation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : gaofee
 * @date : 20:17 2021/1/26
 * @码云地址 : https://gitee.com/itgaofee
 */
@RestController
public class UserController {

    @PostMapping("update")
    @RedisLockTestAnnotation(redisKey = "REDIS_TEST_" + "#0")
    public JSONObject sutdentInfoUpdate(String studentId,int age){
        JSONObject result = new JSONObject();
        result.put("update","success");
        return result;
    }
}
